Feature: Trigger the Post API and validate the Response Body and Request Parameters

@Post_API_TestCases
Scenario: Trigger the API request with valid String Request Body Parameters
		 Given Enter Name and Job in Request Body
		 When send the Request with Payload to the Endpoint
		 Then Validate Status Code
		 And Validate Response Body Parameters

# BDD_Cucumber_API_Framework

Implented test cases using BDD Cucumber Framework.
Executed CRUD API
##Technologies Used
Java
RestAssured Framework
BDD Cucumber Framework
#Instructions on how to set up the project for testing.
Create maven Project in IDE
Name the project as GroupId and ArtifactId
Add dependencies in POM.xml File
*dependencies
RestAssured
TestNG
ApachePoi
Cucumber-Java
Cucumber-jUnit
#Sites
Above Framework is implemented by using https://reqres.in/
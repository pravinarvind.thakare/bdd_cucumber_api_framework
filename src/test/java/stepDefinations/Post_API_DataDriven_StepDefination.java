package stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Post_API_DataDriven_StepDefination {
	
	Response response;
	int statuscode;
	String res_name;
	String res_job;
	String res_id;
	String res_createdAt;
	String requestBody;

	@Given("Enter {string} and {string} in post request body")
	public void enter_and_in_post_request_body(String req_name, String req_job) throws IOException {
	    
		File dir_name = Utility.CreateLogDirectory("Post_API_logs");
		requestBody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \""+ req_job + "\"\r\n" + "}";
		String Endpoint = RequestBody.Hostname() + RequestBody.Resource();
		response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody, Endpoint);
		Utility.evidenceFileCreater(Utility.testLogName("Post_DD_Test_Case_1"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
	    //throw new io.cucumber.java.PendingException();
	}
	@When("Send the post request with data")
	public void send_the_post_request_with_data() {
		statuscode = response.statusCode();
		ResponseBody res_body = response.getBody();
		res_name = res_body.jsonPath().getString("name");
		res_job = res_body.jsonPath().getString("job");
		res_id = res_body.jsonPath().getString("id");
		res_createdAt = res_body.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.substring(0, 11);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate data_driven_post status code")
	public void validate_data_driven_post_status_code() {
		Assert.assertEquals(statuscode, 201);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate data_driven_post response body parameters")
	public void validate_data_driven_post_response_body_parameters() {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expecteddate);

		System.out.println("Post API response validation successful in BDD Cucumber..");
	    //throw new io.cucumber.java.PendingException();
	}
}

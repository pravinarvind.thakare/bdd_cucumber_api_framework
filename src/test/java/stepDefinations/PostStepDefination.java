package stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class PostStepDefination {

	Response response;
	int statuscode;
	String res_name;
	String res_job;
	String res_id;
	String res_createdAt;
	String requestBody;

	@Given("Enter Name and Job in Request Body")
	public void enter_name_and_job_in_request_body() throws IOException {
		// Write code here that turns the phrase above into concrete actions
		File dir_name = Utility.CreateLogDirectory("Post_API_logs");
		requestBody = RequestBody.req_post_tc("Post_TC1");
		String Endpoint = RequestBody.Hostname() + RequestBody.Resource();
		response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody, Endpoint);
		Utility.evidenceFileCreater(Utility.testLogName("Test_Case_1"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());

		// throw new io.cucumber.java.PendingException();
	}

	@When("send the Request with Payload to the Endpoint")
	public void send_the_request_with_payload_to_the_endpoint() {
		// Write code here that turns the phrase above into concrete actions
		statuscode = response.statusCode();
		ResponseBody res_body = response.getBody();
		res_name = res_body.jsonPath().getString("name");
		res_job = res_body.jsonPath().getString("job");
		res_id = res_body.jsonPath().getString("id");
		res_createdAt = res_body.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.substring(0, 11);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Status Code")
	public void validate_status_code() {
		// Write code here that turns the phrase above into concrete actions
		Assert.assertEquals(statuscode, 201);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Response Body Parameters")
	public void validate_response_body_parameters() {
		// Write code here that turns the phrase above into concrete actions
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expecteddate);

		System.out.println("POST API SUCCESSFULLY VALIDATED in BDD_Cucumber...!!!!");
		// throw new io.cucumber.java.PendingException();
	}

}

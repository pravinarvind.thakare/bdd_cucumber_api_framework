package cucumber.Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/Features", glue= {"stepDefinations"}, tags= "@Post_API_TestCases or @Data_Driven")
public class TestRunner {

}

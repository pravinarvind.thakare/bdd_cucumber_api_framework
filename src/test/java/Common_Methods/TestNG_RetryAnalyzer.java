package Common_Methods;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class TestNG_RetryAnalyzer implements IRetryAnalyzer {

	// create counter variables
	private int countStart = 0;
	private int countEnd = 4;

	@Override
	public boolean retry(ITestResult result) {
		if (countStart < countEnd) {
			String testcaseName = result.getName();
			System.out.println(testcaseName + " failed in current interation" + countStart + ", Hence retrying for "
					+ (countStart + 1));
			countStart ++;
			return true;
		}
		return false;
	}

}
